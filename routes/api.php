<?php

use \App\Http\Controllers\Api\TicketController;
use \App\Http\Controllers\Api\ResponderController;

Route::group(['prefix' => 'ticket', 'name' => 'api.ticket.', 'as' => 'api.ticket.'], function(){
    Route::get('/waiting-list', [TicketController::class, 'list'])->name('waitingList');
    Route::post('/store', [TicketController::class, 'store'])->name('store');
    Route::post('/find-waiting-by-phone', [TicketController::class, 'findWaitingByPhoneNumber'])->name('findWaitingByPhone');
    Route::post('/check-in-queue', [TicketController::class, 'ticketCheckInQueue'])->name('checkInQueue');
    Route::post('/set-responder', [TicketController::class, 'ticketSetAResponder'])->name('setAResponder');
    Route::post('/finish-ticket', [TicketController::class, 'ticketSetAsFinished'])->name('setAsFinished');
});

Route::group(['prefix' => 'responder', 'name' => 'api.responder.', 'as' => 'api.responder.'], function(){
    Route::get('/find-free-one', [ResponderController::class, 'findFreeResponder'])->name('freeOne');
});

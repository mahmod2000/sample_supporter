<?php

namespace Database\Seeders;

use App\Models\Manager;
use App\Models\Supervisor;
use App\Models\Supporter;
use Database\Factories\SupervisorFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('managers')->truncate();
        DB::table('supervisors')->truncate();
        DB::table('supporters')->truncate();
        DB::table('tickets')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        \App\Models\Ticket::factory(10)->create();
    }
}

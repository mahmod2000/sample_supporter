<?php

namespace Database\Factories;

use App\Models\Manager;
use App\Models\Supervisor;
use Illuminate\Database\Eloquent\Factories\Factory;

class SupervisorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Supervisor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'manager_id' => Manager::factory(),
            'full_name' => $this->faker->name,
            'email' => $this->faker->email,
            'is_answering' => $this->faker->randomElement([0,1]),
        ];
    }
}

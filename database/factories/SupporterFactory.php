<?php

namespace Database\Factories;

use App\Models\Supervisor;
use App\Models\Supporter;
use Illuminate\Database\Eloquent\Factories\Factory;

class SupporterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Supporter::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'supervisor_id' => Supervisor::factory(),
            'full_name' => $this->faker->name,
            'email' => $this->faker->email,
            'is_answering' => $this->faker->randomElement([0,1]),
        ];
    }
}

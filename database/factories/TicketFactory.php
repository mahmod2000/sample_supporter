<?php

namespace Database\Factories;

use App\Enum\TicketStatus;
use App\Models\Supporter;
use App\Models\Ticket;
use App\Models\Supervisor;
use App\Models\Manager;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ticket::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        /** @var $responderType */
        $responderType = $this->faker->randomElement([
            Supporter::class,
            Supervisor::class,
            Manager::class,
        ]);

        return [
            'phone_number' => $this->faker->phoneNumber,
            'responder_id' => $this->responderId($responderType),
            'responder_type' => $responderType,
            'number_in_line' => $this->faker->randomDigit(),
            'status' => $this->faker->randomElement(TicketStatus::getValues()),
            'point' => $this->faker->randomDigit(),
        ];
    }

    /**
     * @param $responderType
     * @return \Illuminate\Support\HigherOrderCollectionProxy|mixed
     */
    public function responderId($responderType)
    {
        if ($responderType === Supporter::class) return Supporter::factory()->create()->id;
        elseif ($responderType === Supervisor::class) return Supervisor::factory()->create()->id;

        return Manager::factory()->create()->id;
    }
}

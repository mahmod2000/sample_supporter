<?php

namespace App\Enum;


class TicketStatus extends EnumStateManager
{
    const WAITING = 1;
    const ANSWERED = 2;
    const NOT_ANSWERED = 3;

    static protected $names = [
        self::WAITING => 'در انتظار پاسخ',
        self::ANSWERED => 'پاسخ داده شده',
        self::NOT_ANSWERED => 'پاسخ داده نشده',
    ];
}

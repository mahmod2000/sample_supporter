<?php

namespace App\Enum;

abstract class EnumStateManager
{
    static protected $names = [];

    private $enumValue;

    public function __construct($value)
    {
        if ($value instanceof $this) {
            $this->enumValue = $value->getValue();
        } else {
            if (static::isValueValid($value) === false) {
                throw new \Exception('Invalid ' . get_class($this) . ' value ' . $value);
            }
            $this->enumValue = $value;
        }
    }

    public function getValue()
    {
        return $this->enumValue;
    }

    public static function isValueValid($enumValue)
    {
        return isset(static::$names[$enumValue]);
    }

    public static function getName($enumValue, $lang = null, $default = false)
    {
        return isset(static::$names[$enumValue])
            ? __(static::$names[$enumValue], [], $lang)
            : $default;
    }

    public static function getNames($values = null)
    {
        if ($values === null) {
            return static::$names;
        }
        return array_intersect_key(static::$names, array_flip($values));
    }

    public static function getValueByName($enumName = null, $default = false)
    {
        $array = isset(static::$_names) ? array_flip(static::$_names) : [];

        return isset($array[$enumName]) ? $array[$enumName] : $default;
    }

    public static function getCount()
    {
        return count(static::$names);
    }

    public static function getValues()
    {
        return array_keys(static::$names);
    }
}

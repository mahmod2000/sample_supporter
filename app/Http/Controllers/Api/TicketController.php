<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Ticket\TicketCheckQueueRequest;
use App\Http\Requests\Ticket\TicketFindByPhoneNumberRequest;
use App\Http\Requests\Ticket\TicketFinishRequest;
use App\Http\Requests\Ticket\TicketSetResponderRequest;
use App\Http\Requests\Ticket\TicketStoreRequest;
use App\Http\Resources\TicketResource;
use App\Repositories\TicketRepository;

class TicketController extends Controller
{
    /**
     * @var TicketRepository
     */
    private TicketRepository $ticketRepository;

    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * List of waiting tickets and order by (number in line) column
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $tickets = $this->ticketRepository->listByNumberInLine();
        return $this->sendResponseJson(TicketResource::collection($tickets));
    }

    /**
     * Adding new ticket
     * It just add one phone number with one status = 1 (waiting)
     * @param TicketStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(TicketStoreRequest $request): \Illuminate\Http\JsonResponse
    {
        $ticket = $this->ticketRepository->addNew($request->all());
        return $this->sendResponseJson(new TicketResource($ticket));
    }

    /**
     * @param TicketFindByPhoneNumberRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findWaitingByPhoneNumber(TicketFindByPhoneNumberRequest $request): \Illuminate\Http\JsonResponse
    {
        $ticket = $this->ticketRepository->findByPhoneNumberInWaiting($request->get('phone_number'));
        if(empty($ticket)) {
            return $this->sendResponseJson([],false, 'Ticket not found!');
        }
        return $this->sendResponseJson(new TicketResource($ticket));
    }

    /**
     * It can check user queue in line and update number_in_line based on users in queue
     * @param TicketCheckQueueRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function ticketCheckInQueue(TicketCheckQueueRequest $request): \Illuminate\Http\JsonResponse
    {
        $ticket = $this->ticketRepository->checkingInQueue($request->get('ticket_id'));
        return $this->sendResponseJson(new TicketResource($ticket));
    }

    /**
     * Setting a responder for ticket
     * @param TicketSetResponderRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function ticketSetAResponder(TicketSetResponderRequest $request): \Illuminate\Http\JsonResponse
    {
        $ticket = $this->ticketRepository->setAResponder($request->get('ticket_id'), $request->get('responder_id'), $request->get('responder_type'));
        return $this->sendResponseJson(new TicketResource($ticket));
    }

    /**
     * Adding new ticket
     * @param TicketFinishRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function ticketSetAsFinished(TicketFinishRequest $request): \Illuminate\Http\JsonResponse
    {
        /** Finish ticket */
        $ticket = $this->ticketRepository->finishATicket($request->get('ticket_id'), $request->get('answered'), $request->get('point'));
        return $this->sendResponseJson(new TicketResource($ticket));
    }
}

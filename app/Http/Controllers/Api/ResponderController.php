<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\ManagerRepository;
use App\Repositories\SupervisorRepository;
use App\Repositories\SupporterRepository;
use App\Repositories\TicketRepository;
use Illuminate\Http\Request;

class ResponderController extends Controller
{
    /**
     * @var ManagerRepository
     */
    private ManagerRepository $managerRepository;

    /**
     * @var SupervisorRepository
     */
    private SupervisorRepository $supervisorRepository;

    /**
     * @var SupporterRepository
     */
    private SupporterRepository $supporterRepository;

    /**
     * @var TicketRepository
     */
    private TicketRepository $ticketRepository;

    public function __construct(ManagerRepository $managerRepository,
                                SupervisorRepository $supervisorRepository,
                                SupporterRepository $supporterRepository,
                                TicketRepository $ticketRepository)
    {

        $this->managerRepository = $managerRepository;
        $this->supervisorRepository = $supervisorRepository;
        $this->supporterRepository = $supporterRepository;
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * Finding the free person in teams and if there is any one,
       So in the response we pass a first_ticket_in_line to see the user's / ticket's info that is in the first in line
     * Finding free responder
     * @return \Illuminate\Http\JsonResponse
     */
    public function findFreeResponder(): \Illuminate\Http\JsonResponse
    {
        /**
         * it has default values
         * @var $data
         */
        $data = [
            'responder_id' => 0,
            'responder_type' => null
        ];

        /** @var $message */
        $message = 'There is no responder to answer to the user yet!';

        if(!empty($supporter = $this->supporterRepository->findFirstByAnswerStatus())) {
            $data = [
                'responder_id' => $supporter->id,
                'responder_type' => 'supporter'
            ];
        } else if(!empty($supervisor = $this->supervisorRepository->findFirstByAnswerStatus())) {
            $data = [
                'responder_id' => $supervisor->id,
                'responder_type' => 'supervisor'
            ];
        } else if(!empty($manager = $this->managerRepository->findFirstByAnswerStatus())) {
            $data = [
                'responder_id' => $manager->id,
                'responder_type' => 'manager'
            ];
        }

        /** If there is one person to answer, so we can find a user/ticket that is the first one in line */
        if($data['responder_id'] > 0) {
            $data['first_ticket_in_line'] = $this->ticketRepository->findFirstWaitingUser();
            $message = "One person is free in the {$data['responder_type']} team and can answer to the user/ticket.";
        }

        return $this->sendResponseJson($data, true, $message);
    }
}

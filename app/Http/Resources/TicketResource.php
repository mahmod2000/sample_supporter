<?php

namespace App\Http\Resources;

use App\Enum\TicketStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "phone_number" => $this->phone_number,
            "number_in_line" => $this->number_in_line,
            "responder_id" => $this->responder_id,
            "responder_type" => $this->responder_type,
            "point" => $this->point,
            "status" => $this->status,
            "status_text" => $this->when($this->status, TicketStatus::getName($this->status)),
            "created_at" => optional($this->created_at)->format('Y-m-d H:i:s'),
        ];
    }
}

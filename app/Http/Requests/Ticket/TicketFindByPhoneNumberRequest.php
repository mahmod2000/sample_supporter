<?php

namespace App\Http\Requests\Ticket;

use Illuminate\Foundation\Http\FormRequest;

class TicketFindByPhoneNumberRequest extends FormRequest
{
    public function rules()
    {
        return [
            'phone_number' => [
                'required',
                'max:20',
                'min:11',
            ] // For Mobile or Phone! We can set a regex for mobile in the future
        ];
    }
}

<?php

namespace App\Http\Requests\Ticket;

use Illuminate\Foundation\Http\FormRequest;

class TicketCheckQueueRequest extends FormRequest
{
    public function rules()
    {
        return [
            'ticket_id' => ['required', 'exists:tickets,id'],
        ];
    }
}

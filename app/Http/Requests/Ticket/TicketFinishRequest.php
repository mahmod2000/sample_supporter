<?php

namespace App\Http\Requests\Ticket;

use App\Enum\TicketStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TicketFinishRequest extends FormRequest
{
    public function rules()
    {
        return [
            'ticket_id' => ['required', Rule::exists('tickets', 'id')->where('status', TicketStatus::WAITING)],
            'answered' => ['required', 'in:1,0'],
            'point' => ['nullable', 'numeric']
        ];
    }
}

<?php

namespace App\Http\Requests\Ticket;

use App\Enum\TicketStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TicketStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'phone_number' => [
                'required',
                'max:20',
                'min:11',
                Rule::unique('tickets')->where('phone_number', request('phone_number'))->where('status', TicketStatus::WAITING)
            ] // For Mobile or Phone! We can set a regex for mobile in the future
        ];
    }
}

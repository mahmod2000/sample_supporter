<?php

namespace App\Http\Requests\Ticket;

use App\Enum\TicketStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TicketSetResponderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'ticket_id' => [
                'required',
                Rule::exists('tickets', 'id')->where('status', TicketStatus::WAITING)->whereNull('responder_id')
            ],
            'responder_id' => ['required', 'int'],
            'responder_type' => ['required', 'in:supporter,supervisor,manager'],
        ];
    }

    public function messages()
    {
        return [
            'ticket_id.exists' => 'تیکت مورد نظر یافت نشد٬ آیتم یا در وضعیت (در انتظار پاسخ) نیست و یا یک پاسخگو بهش اختصاص داده شده.'
        ];
    }
}

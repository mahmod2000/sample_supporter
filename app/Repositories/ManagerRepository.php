<?php

namespace App\Repositories;

use App\Models\Manager;
use App\Repositories\Traits\AnswerColumnTrait;

class ManagerRepository extends BaseRepository
{
    use AnswerColumnTrait;

    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @return string
     */
    public function model()
    {
        return Manager::class;
    }
}

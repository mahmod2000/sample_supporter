<?php

namespace App\Repositories;

use App\Enum\TicketStatus;
use App\Models\Manager;
use App\Models\Supervisor;
use App\Models\Supporter;
use App\Models\Ticket;
use Illuminate\Support\Facades\DB;

class TicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [

    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * @return string
     */
    public function model()
    {
        return Ticket::class;
    }

    /**
     * @param array $input
     * @return Ticket
     * @throws \Exception
     */
    public function addNew(array $input = []) : Ticket
    {
        DB::beginTransaction();

        try {
            $input['number_in_line'] = $this->getNumberInLineCount() + 1;
            $input['status'] = TicketStatus::WAITING;

            $item = $this->create($input);
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }

        // Run an event like:
        // event(new \App\Events\TicketCreated($item));

        // then commit
        DB::commit();

        return $item;
    }

    /**
     * Finding number in line users
     * @param null $ticket_id
     * @return mixed
     */
    public function getNumberInLineCount($ticket_id = null)
    {
        return $this->model::findWaiting()
            ->select('id')
            ->when($ticket_id, function ($q) use($ticket_id){
                $q->where('id', '<>', $ticket_id);
            })
            ->count();
    }

    /**
     * Finish a ticket as answered or not answered
     * @param $id
     * @param int $answered
     * @param int $point
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     * @return Ticket
     */
    public function finishATicket($id, $answered = 1, $point = 0) : Ticket
    {
        DB::beginTransaction();

        try {
            $ticket = $this->model::where('id', $id)->select('id','responder_id','responder_type','status','point')->with('responder')->firstOrFail();

            /** Update ticket */
            $ticket = $this->update([
                'status' => $answered ? TicketStatus::ANSWERED : TicketStatus::NOT_ANSWERED,
                'point' => $point
            ], $id);

            /** Then we should set Responder answering status to 0 */
            if(!empty($ticket->responder)) $ticket->responder()->update(['is_answering' => 0]);
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }

        /*** We can run an event like: event(new \App\Events\TicketFinished($item)); ***/

        DB::commit();

        return $ticket;
    }

    /**
     * @param $id
     * @param $responder_id
     * @param $responder_type
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     * @throws \Throwable
     */
    public function setAResponder($id, $responder_id, $responder_type)
    {
        if($responder_type == 'supporter') $responder_type = Supporter::class;
        elseif($responder_type == 'supervisor') $responder_type = Supervisor::class;
        else $responder_type = Manager::class;

        /** @var $check_responder */
        $check_responder = $responder_type::select('id','is_answering')->whereId($responder_id)->where('is_answering', 0)->count();
        throw_if(!$check_responder, new \Exception('The Responder not found.'));

        DB::beginTransaction();
        try {
            /** Update ticket */
            $ticket = $this->update(['responder_id' => $responder_id, 'responder_type' => $responder_type], $id);

            /** Update responder is_answering to 1 */
            $ticket->responder()->update(['is_answering' => 1]);
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }

        DB::commit();

        return $ticket;
    }

    /**
     * @param $id
     * @return Ticket
     * @throws \Exception
     */
    public function checkingInQueue($id) : Ticket
    {
        DB::beginTransaction();

        try {
            /** @var $ticket */
            $ticket = $this->findOrFail($id, ['id','number_in_line']);

            /** @var $count_number_in_line */
            $count_number_in_line = $this->getNumberInLineCount($id) + 1;

            $ticket = $this->update(['number_in_line' => $count_number_in_line], $ticket->id);
        } catch (\Exception $e) {
            DB::rollBack();

            throw new \Exception($e->getMessage());
        }

        DB::commit();

        return $ticket;
    }

    /**
     * @return mixed
     */
    public function listByNumberInLine()
    {
        return $this->model::select('id','phone_number','number_in_line','responder_id','responder_type','status','created_at')
            ->findWaiting()
            ->orderBy('number_in_line', 'asc')
            ->get();
    }

    /**
     * Finding the first user in queue
     * @return mixed
     */
    public function findFirstWaitingUser()
    {
        return $this
            ->model::select('id','phone_number','number_in_line','responder_id','responder_type','status','created_at')
            ->findWaiting()
            ->orderBy('number_in_line', 'asc')
            ->first();
    }

    /**
     * Finding by phone number
     * @param $phone_number
     * @return mixed
     */
    public function findByPhoneNumberInWaiting($phone_number)
    {
        return $this
            ->model::select('id','phone_number','number_in_line','responder_id','responder_type','status','created_at')
            ->findWaiting()
            ->where('phone_number', $phone_number)
            ->first();
    }
}

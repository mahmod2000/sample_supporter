<?php

namespace App\Repositories\Traits;

trait AnswerColumnTrait
{
    /**
     * @param $id
     * @param $value
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model
     * @throws \Exception
     */
    public function updateIsAnswering($id, $value = 1)
    {
        return $this->update(['is_answering' => $value], $id);
    }

    /**
     * @param int $value
     * @return mixed
     */
    public function findFirstByAnswerStatus($value = 0)
    {
        return $this->model::where('is_answering', $value)
            ->select('id','is_answering')
            ->first();
    }
}

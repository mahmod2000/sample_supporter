<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Supervisor
 * @package App\Models
 *
 * @property int $id
 * @property int $manager_id
 * @property string $full_name
 * @property string $email
 * @property boolean $is_answering
 * @property datetime $created_at
 * @property datetime $updated_at
 */
class Supervisor extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'manager_id',
        'full_name',
        'email',
        'is_answering'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function supporters()
    {
        return $this->hasMany(Supporter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }
}

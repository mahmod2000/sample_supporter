<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Manager
 * @package App\Models
 *
 * @property string $full_name
 * @property string $email
 * @property boolean $is_answering
 * @property datetime $created_at
 * @property datetime $updated_at
 */
class Manager extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'full_name',
        'email',
        'is_answering'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function supervisors()
    {
        return $this->hasMany(Supervisor::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Supporter
 * @package App\Models
 *
 * @property int $id
 * @property int $supervisor_id
 * @property string $full_name
 * @property string $email
 * @property boolean $is_answering
 * @property datetime $created_at
 * @property datetime $updated_at
 */
class Supporter extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'supervisor_id',
        'full_name',
        'email',
        'is_answering'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supervisor()
    {
        return $this->belongsTo(Supervisor::class);
    }
}

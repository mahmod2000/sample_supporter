<?php

namespace App\Models;

use App\Enum\TicketStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ticket
 * @package App\Models
 *
 * @property int $id
 * @property string $phone_number
 * @property string $responder_type
 * @property int $responder_id
 * @property int $number_in_line
 * @property int $status
 * @property int $point
 * @property datetime $created_at
 * @property datetime $updated_at
 */
class Ticket extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'phone_number',
        'responder_type',
        'responder_id',
        'number_in_line',
        'status',
        'point',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function responder()
    {
        return $this->morphTo();
    }

    /**
     * @param $query
     */
    public function scopeFindWaiting($query)
    {
        $query->where('status', TicketStatus::WAITING);
    }
}
